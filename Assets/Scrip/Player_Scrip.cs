using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Scrip : MonoBehaviour
{
    [SerializeField]
    private float walkSpeed = 5f;
    private float xAxis;
    private Rigidbody2D rb2d;

    public LayerMask groundMask;
    public bool isGrounded;

    private float jumpForce = 800;
    public bool isJumpPressed;

    //Animation Controller
    private Animator animator;
    public AnimationStates currentAnimaton;

    //Component
    private BoxCollider2D boxColider_2D;
 

    //=====================================================
    // Start is called before the first frame update
    //=====================================================
    void Start()
    {
        //OLD
        //*****rb2d = GetComponent<Rigidbody2D>();
        //groundMask = 1 << LayerMask.NameToLayer("Ground");

        //ADD
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        boxColider_2D = GetComponent<BoxCollider2D>();

    }

    //=====================================================
    // Update is called once per frame
    //=====================================================
    void Update()
    {

        //Checking for inputs
        xAxis = Input.GetAxisRaw("Horizontal");

        //space jump key pressed?
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            isJumpPressed = true;
            isGrounded = false;
        }

  
    }

    //=====================================================
    // Physics based time step loop
    //=====================================================
    private void FixedUpdate()
    {
        //check if player is on the ground
        UpdateGround();

        //------------------------------------------
        //Check update movement based on input
        Vector2 vel = UpdateMovement();

        //Animation Update
        UpdateAnimation();

        //------------------------------------------
        //Check if trying to jump 
        LateUpdateInput();

        //assign the new velocity to the rigidbody
        rb2d.velocity = vel;

        
    }

    void UpdateGround()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 0.1f, groundMask);

        if (hit.collider != null)
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }

    Vector2 UpdateMovement()
    {
        Vector2 vel = new Vector2(0, rb2d.velocity.y);

        if (xAxis < 0)
        {
            vel.x = -walkSpeed;
            transform.localScale = new Vector2(-3, 3);
        }
        else if (xAxis > 0)
        {
            vel.x = walkSpeed;
            transform.localScale = new Vector2(3, 3);

        }
        else
        {
            vel.x = 0;
        }


        return vel;
    }

    void LateUpdateInput()
    {
        if (isJumpPressed && isGrounded)
        {
            rb2d.AddForce(new Vector2(0, jumpForce));
            //boxColider_2D.enabled = !boxColider_2D.enabled;
            isJumpPressed = false;
            // animate  jump

        }
    }

    //=====================================================
    // mini animation manager
    //=====================================================
    void ChangeAnimationState(AnimationStates newAnimation)
    {
        if (currentAnimaton == newAnimation) return;

        animator.Play(newAnimation.ToString());
        currentAnimaton = newAnimation;
    }

    //=====================================================
    // UpdateAnimation
    //=====================================================

    void UpdateAnimation()
    {
        if (isGrounded)
        {
            if (xAxis != 0)
            {
                ChangeAnimationState(AnimationStates.Player_run);
            }
            else
            {
                ChangeAnimationState(AnimationStates.Player_idle);
            }
        }

        else
        {
            ChangeAnimationState(AnimationStates.Player_jump);
        }

    }
}

